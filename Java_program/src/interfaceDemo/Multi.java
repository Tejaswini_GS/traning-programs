package interfaceDemo;
// Multiple inheritance achieved with interfaces
interface Continent{
	void continentName();  // abstract method 
}
interface Country{
	void countryName();  // abstract method 
}
interface City{
	void cityName();  // abstract method 
}
public class Multi implements Continent, Country, City {
	@Override
	public void cityName() {
		System.out.println("Copenhagen");
		}
	@Override
	public void countryName() {
		System.out.println("Denmark");
		}
	@Override
	public void continentName() {
		System.out.println("Europe");
		}
	public static void main(String[] args) {
		
		Multi m = new Multi();
		m.cityName();
		m.countryName();
		m.continentName();
	}
	}
	



