package interfaceDemo;

public class B implements A{

	@Override
	public void method1() {  // abstract metho from interface A 
		System.out.println("This is a interface method1.... ");
		
	}

	@Override
	public void method2() {  // abstract metho from interface A 
		System.out.println("MEthod 2  .. This is a interface method1.... ");
	
	}
	void thanks() {   // its my own method 
		System.out.println("This is my own method... thanks interface A ");
	}
	public static void main(String[] args) {
		     B obj = new B();
		     obj.method1();
		     obj.method2();
		     obj.thanks();
		     
	}

}
