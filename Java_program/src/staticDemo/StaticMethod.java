package staticDemo;

public class StaticMethod {
	
	 void play() {  // regular method
		 System.out.println("  I play TENNIOs...........");
	 }
	 // static method
	 static void sleep() {
		 System.out.println("I sleep at 9.........");// no need to create object to call static method 
	 }

	public static void main(String[] args) {
		sleep();
		StaticMethod sm= new StaticMethod();
		sm.play();		
            
	}

}
