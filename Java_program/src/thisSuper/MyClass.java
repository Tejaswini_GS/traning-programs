package thisSuper;

class MyParent{
	String name="This is Parent class instance variable ...";
	
}

public class MyClass extends MyParent{
	String name="This is instance variable";

	
	void somemethod() {
		String name= "This is local varible ... because I am inside method ";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	 
	}

	public static void main(String[] args) {
		MyClass mc=new MyClass();
		mc.somemethod();

	}

}
