package thisSuper;

class Animal{
	String color="WHITE.. PARENT CLASS"; // parent class instance variable
}


public class Dog extends Animal{
	
	String color="GREEN.....";  // current or present  class instance variable 
	
	void printcolor() {
		String color="BLACK.....";  // local variable
		System.out.println(color);   // it prints local variable 
		System.out.println(this.color); // it prints current class instance variable 
		System.out.println(super.color); //
	}

	public static void main(String[] args) {
		
		Dog d = new Dog();
		d.printcolor();
	}

}
