package thisSuper;


class Parent{
	void play() {
		System.out.println(" Parent play carroms...");
	}
}
public class ThisSuperMethodDemo extends Parent {
              void play() {
            	  System.out.println(" I Play Tennis.....  current class");
              }
              void all() {  // calling current class play method and also parent class play method 
            	 
            	  super.play();  // parent class play method 
            	  this.play();  // current class play method 
              }
	public static void main(String[] args) {
		ThisSuperMethodDemo obj = new ThisSuperMethodDemo();
		obj.all();
		
      
	}

}

