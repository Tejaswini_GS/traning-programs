package Abstraction;

public abstract class Animal {  // its abstract class.... 
	// abstract method creation
	 abstract void methodA();   // no method body
	 void regularMethod() {    // regular method
		 System.out.println("This is my regular method ...");
	 }
	 
	 }

