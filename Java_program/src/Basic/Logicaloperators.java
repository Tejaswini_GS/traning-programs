package Basic;

public class Logicaloperators {

	public static void main(String[] args) {
		// && AND - both conditions should be true
		System.out.println((100>50)&& (20>10));
		System.out.println((200<50)&& (300<10));
		
		// || OR - if anyone conditions is true then its true
		
		System.out.println((100>50) || (20>10));
		System.out.println((200<50) || (300<10));
	}

}
