package inheritance;

public class Grandchild extends Child1{
	
	public void gcmethod() {
		System.out.println("Grand CHILD method logic ...");
	}

	public static void main(String[] args) {
		Grandchild g = new Grandchild();
		g.method1();  // method from parent 
		g.method2();  // method from parent
		g.gcmethod(); // method from child class 

	}

}
