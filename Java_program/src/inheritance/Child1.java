package inheritance;

public class Child1 extends Parent{   // inherited parent class 

	public void mymethod() {   // its own method 
		System.out.println("This is CHILD CLASS method ....");
	}
	
	public static void main(String[] args) {
		
		Child1 c = new Child1();  // object created 
		c.method1();  // calling parent method 
		c.method2();  // calling second method 

	}

}
